package inc.vars.recorder_sample;

import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import static inc.vars.recorder_sample.IncomingCallReceiver.endCall;

public class SubWindowFlagment extends Fragment {

    public View view;
    private MainActivity _activity;

    /*
     * 画面呼び出し
     */
    public View loadView(MainActivity mainActivity) {
        _activity = mainActivity;

        LayoutInflater inflater = LayoutInflater.from(_activity);
        view =  inflater.inflate(R.layout.sub_window_fragment, null);

        // メインウィンドウを閉じるボタン
        Button windowButton = (Button)view.findViewById(R.id.closeMainButton);
        windowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeMainWindow();
            }
        });

        // 常駐アプリを閉じるボタン
        Button closeButton = (Button)view.findViewById(R.id.closeSubButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeSubWindow();
            }
        });

        Button recStartButton = (Button)view.findViewById(R.id.recStartButton);
        recStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recStart();
            }
        });

        Button recEndButton = (Button)view.findViewById(R.id.recEndButton);
        recEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recEnd();
            }
        });

        Button callEndButton = (Button)view.findViewById(R.id.callEndButton);
        callEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callEnd();
            }
        });

        return view;
    }

    /*
     * メインウィンドウを閉じる
     */
    private void closeMainWindow() {
        _activity.moveTaskToBack(true);
    }

    /*
     * サブウィンドウを閉じる
     */
    private void closeSubWindow() {
        _activity.closeSubWindow();
    }

    /*
     * 録音を開始する
     */
    private void recStart() {
        _activity.recStart();
    }
    /*
     * 録音を終了する
     */
    private void recEnd() {
        _activity.recEnd();
    }

    public void callEnd() {
        _activity.callEnd();
    }
}
