package inc.vars.recorder_sample;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class IncomingCallReceiver extends BroadcastReceiver {

    private Method _m1;
    private Method _m2;
    private static Method _m3;
    private static Object _iTelephony;

    public IncomingCallReceiver() {
        _m1 = null;
        _m2 = null;
        _m3 = null;
        _iTelephony = null;
    }

    @SuppressLint({"UnsafeProtectedBroadcastReceiver", "PrivateApi"})
    @Override
    public void onReceive(Context context, Intent intent) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            _m1 = tm.getClass().getDeclaredMethod("getITelephony");
            _m1.setAccessible(true);
            _iTelephony = _m1.invoke(tm);
            _m2 = _iTelephony.getClass().getDeclaredMethod("silenceRinger");
            _m3 = _iTelephony.getClass().getDeclaredMethod("endCall");
        } catch (Exception e) {
            e.printStackTrace();
        }

        tm.listen(new PhoneStateListener(){
            // PhoneStateListenerの`onCallStateChanged`をOverride
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                // パーミッションがない場合、incomingNumberは常に空
                switch (state) {
                    case TelephonyManager.CALL_STATE_RINGING:
                        /* 着信 */
                        Log.i("call status", "着信");
                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        /* 通話 */
                        Log.i("call status", "通話");


                        if(!(TelephonyManager.CALL_STATE_IDLE == state)){
                            Log.i("call status", "通話終了");
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        /* 待受 */
                        Log.i("call status", "待受");
                        break;

                }
            }
        }, PhoneStateListener.LISTEN_CALL_STATE);

//        String intentAction = intent.getAction();
//        MainActivity.listenEndCall();

    }

    public void silenceRinger() throws InvocationTargetException, IllegalAccessException {
        _m2.invoke(_iTelephony);
        Log.d("rec", "silenceRinger");
    }

    public static void endCall() {

        try {
            _m3.invoke(_iTelephony);
        } catch(NullPointerException e){
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Log.d("rec", "endCall");
    }
}
