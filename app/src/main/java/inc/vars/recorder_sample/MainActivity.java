package inc.vars.recorder_sample;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.provider.CallLog;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static inc.vars.recorder_sample.R.id.callEndButton;


/*
 * 残念ながらAndroidスマホでは、全ての機種に音声を録音することができるアプリが標準搭載されているわけではありません。（一部の機種には、音声を録音できるアプリがあらかじめ用意されています）
 * via https://kanntann.com/what-is-a-voice-recorder
 * 本サンプルでは、getExternalStorageDirectory (/storage/emulated/0)を保存先にしています。
 *
 * andoroid8でアプリから電源を落とす方法
 * android.permission.MODIFY_PHONE_STATEが必要
 * ただ、このMODIFY_PHONE_STATEは以下の成約があります。
 *  - ROMのシステムフォルダにあらかじめインストールされている
 *  - 製造元がセキュリティ証明書を使用してコンパイルしたもの
 *  https://code.i-harness.com/ja/q/47f2f2
 * 現在のソースでは受話中にサブウィンドウのCALL ENDを押下する頃で、
 * java.lang.SecurityException: MODIFY_PHONE_STATE permission required.が発生する事から、
 * 証明書ありのappであればダイアログ表示されるのではと想定されます。
 *
 *
 */
public class MainActivity extends AppCompatActivity {

    private WindowManager _windowManager;
    private SubWindowFlagment _subWindowFragment;
    private SubWindowService _subWindowService;
//    private static final int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 22;
    private static final int PERMISSIONS_REQUEST = 20022;
//    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 30022;
    MediaRecorder recorder;
//    MediaRecorder recorder_uplink;
//    MediaRecorder recorder_downlink;

    /*
     * メインの画面作成
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        Button windowButton = (Button)this.findViewById(R.id.openWindowButton);
        windowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSubWindow();
            }
        });

        checkPermission();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void checkPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission_group.STORAGE) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.MODIFY_PHONE_STATE) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAPTURE_AUDIO_OUTPUT) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED
                ) {
            // 許可されてない
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission_group.STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                    Manifest.permission.CAMERA,
                    Manifest.permission.MODIFY_PHONE_STATE,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.CAPTURE_AUDIO_OUTPUT,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.READ_CALL_LOG,
                    Manifest.permission.INTERNET
            }, PERMISSIONS_REQUEST);

            getCallDetails((TextView) this.findViewById(R.id.call));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
//            case PERMISSIONS_REQUEST_GROUP_STORAGE:
//            case PERMISSIONS_REQUEST_RECORD_AUDIO:

        }
    }

    /*
     * サブウィンドウを作成
     */
    public void openSubWindow() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow 以上用の処理
            if (Settings.canDrawOverlays(this)) {
                if (_subWindowFragment != null && _subWindowFragment.view != null) {
                    closeSubWindow();
                }

                //oreo以上は TYPE_SYSTEM_ALERT でなく TYPE_APPLICATION_OVERLAYを利用する
                if(Build.VERSION.SDK_INT >= 26){
                    WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                            WindowManager.LayoutParams.WRAP_CONTENT,
                            WindowManager.LayoutParams.WRAP_CONTENT,
                            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN |
                                    WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                            PixelFormat.TRANSLUCENT
                    );
                    params.gravity = Gravity.TOP | Gravity.LEFT;

                    _windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
                    _subWindowFragment = new SubWindowFlagment();
                    _windowManager.addView(_subWindowFragment.loadView(this), params);

                }else{
                    WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                            WindowManager.LayoutParams.WRAP_CONTENT,
                            WindowManager.LayoutParams.WRAP_CONTENT,
                            WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN |
                                    WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                            PixelFormat.TRANSLUCENT
                    );
                    params.gravity = Gravity.TOP | Gravity.LEFT;

                    _windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
                    _subWindowFragment = new SubWindowFlagment();
                    _windowManager.addView(_subWindowFragment.loadView(this), params);
                }

            } else {
                // サブウィンドウ生成の権限がなかった場合は、下記で権限を取得する
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:"+ getPackageName()));
                this.startActivityForResult(intent, 22);
            }
        } else {
            // Lollipop 以前用の処理
            startService(new Intent(MainActivity.this, SubWindowService.class));
            // Binder によるサブウィンドウとの接続
            bindService(new Intent(MainActivity.this, SubWindowService.class), _connection, Context.BIND_ABOVE_CLIENT);
        }
    }


    // Build.VERSION.SDK_INT < 22 の対応
    private ServiceConnection _connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            _subWindowService = ((SubWindowService.SubWindowServiceBinder)iBinder).getService();
            openWindow();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {}
    };
    public void openWindow() {

        _subWindowService.openWindow(this);
    }

    // Build.VERSION.SDK_INT >= 23 の対応
    public void closeSubWindow() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (_subWindowFragment != null && _subWindowFragment.view != null) {
                _windowManager.removeView(_subWindowFragment.view);
                _subWindowFragment.view = null;
            }
        } else {
            _subWindowService.closeSubWindow();
            stopService(new Intent(MainActivity.this, SubWindowService.class));
        }
    }

    public void recStart(){

        /*
         * VOICE_CALLは VOICE_COMMUNICATIONに統合される？
         * https://stackoverflow.com/questions/45098103/how-to-record-voice-call-using-audiosource-voice-call
         * でもそんな事は以下には書いていない
         * https://developer.android.com/reference/android/media/MediaRecorder.AudioSource
         * ただ、VOICE_CALLを利用すると、[E/MediaRecorder: start failed: -2147483648]が発生
         */

        if (Build.VERSION.SDK_INT >= 15) {
            Log.d("rec", "15👆");
            recorder = new MediaRecorder();
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

//            recorder.setAudioSamplingRate(44100);
//            recorder.setAudioEncodingBitRate(96000);
//            recorder.setAudioSamplingRate(48000);
//            recorder.setAudioEncodingBitRate(96000);
            recorder.setAudioChannels(1);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//            recorder.setAudioSamplingRate(8000);
//            recorder.setAudioEncodingBitRate(12200);
//            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            //保存先
            String filePath = Environment.getExternalStorageDirectory() + "/"+getNowDate()+".mp4";
            Log.d("rec", Environment.getExternalStorageDirectory().getAbsolutePath());
            recorder.setOutputFile(filePath);

//            Log.d("rec", "15👆");
//
//            recorder_uplink = new MediaRecorder();
//            recorder_uplink.setAudioSource(MediaRecorder.AudioSource.MIC);
//            recorder_uplink.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//            recorder_uplink.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//            //保存先
//            String filePath_up = Environment.getExternalStorageDirectory() + "/"+getNowDate()+"_uplink.mp4";
//            Log.d("rec", Environment.getExternalStorageDirectory().getAbsolutePath());
//            recorder_uplink.setOutputFile(filePath_up);
//
//            recorder_downlink = new MediaRecorder();
//            recorder_downlink.setAudioSource(MediaRecorder.AudioSource.VOICE_DOWNLINK);
//            recorder_downlink.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//            recorder_downlink.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//            //保存先
//            String filePath_down = Environment.getExternalStorageDirectory() + "/"+getNowDate()+"_downlink.mp4";
//            Log.d("rec", Environment.getExternalStorageDirectory().getAbsolutePath());
//            recorder_downlink.setOutputFile(filePath_down);
        }
//        else {
//            // older version of Android, use crappy sounding voice codec
//            Log.d("rec", "15👇");
//            recorder.setAudioSamplingRate(8000);
//            recorder.setAudioEncodingBitRate(12200);
//            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//            //保存先
//            String filePath = Environment.getExternalStorageDirectory() + "/audio.3gp";
//            recorder.setOutputFile(filePath);
//        }

        //録音準備＆録音開始
        try {
//            recorder_uplink.prepare();
//            recorder_downlink.prepare();
            recorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
//            recorder_uplink.start();   //録音開始
//            recorder_downlink.start();   //録音開始
            recorder.start();   //録音開始
            Log.d("rec", "rec start");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public  void recEnd(){

        try {
            Log.d("rec", "rec end");
//            recorder_uplink.stop();//録音終了
//            recorder_uplink.reset();
//            recorder_uplink.release();
//            recorder_downlink.stop();//録音終了
//            recorder_downlink.reset();
//            recorder_downlink.release();
            recorder.stop();//録音終了
            recorder.reset();
            recorder.release();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /*
     * Lollipop までは、メインウィンドウをバックグランドに入れる際に onDestroy が実行されてしまう
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Build.VERSION.SDK_INT >= 23) {
            closeSubWindow();
        }
    }

    public static String getNowDate(){
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        final Date date = new Date(System.currentTimeMillis());
        return df.format(date);
    }

    public void callEnd() {
        Log.d("rec", "hogfehoge");
        IncomingCallReceiver.endCall();
    }

    private void getCallDetails(TextView call) {

        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = managedQuery( CallLog.Calls.CONTENT_URI,null, null,null, null);
        int number = managedCursor.getColumnIndex( CallLog.Calls.NUMBER );
        int type = managedCursor.getColumnIndex( CallLog.Calls.TYPE );
        int date = managedCursor.getColumnIndex( CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex( CallLog.Calls.DURATION);
        sb.append( "履歴 :");
        while ( managedCursor.moveToNext() ) {
            String phNumber = managedCursor.getString( number );
            String callType = managedCursor.getString( type );
            String callDate = managedCursor.getString( date );
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString( duration );
            String dir = null;
            int dircode = Integer.parseInt( callType );
            switch( dircode ) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "発信";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "着信";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "キャンセル";
                    break;
                case CallLog.Calls.REJECTED_TYPE:
                    dir = "拒否";
                    break;
            }
            sb.append( "\n電話番号:--- "+phNumber +" \n種別:--- "+dir+" \n日時:--- "+callDayTime+" \n通話時間 :--- "+callDuration );
            sb.append("\n----------------------------------");
        }
//        managedCursor.close();

        call.setText(sb);
    }
}
